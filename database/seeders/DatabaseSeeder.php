<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        \DB::table('products')->insert([
            [
                'name'=>'Flour',


            ],



        ]);
        \DB::table('units')->insert([
            [
                'name' => 'Gram',
                'modifier' => 1,
            ],

            [
                'name' => 'Kilo Gram',
                'modifier' => 1000,
            ],
            [
                'name' => 'Centi Gram',
                'modifier' => 1/100,
            ],
            [
                'name' => 'Kilo Gram',
                'modifier' => 1000,
            ],
            [
                'name' => 'Pound',
                'modifier' => 453.59237
            ]

        ]);
        \DB::table('product_unit')->insert([
                [
                    'product_id' => 1,
                    'unit_id' => 1,
                    'amount' => 1,
                ]

        ]);
        \DB::table('images')->insert([
            [
                'o_id' => 1,
                'o_type' => 'product',
                'path' => 'apple.jpg',
                'description' => 'image of an apple',
                'imageable_id'  =>1,
                'imageable_type' => 'product'
            ]

    ]);
    \DB::table('images')->insert([
        [
            'o_id' => 1,
            'o_type' => 'user',
            'path' => 'bilal.jpg',
            'description' => 'image of a Bilal',

        ]

]);


    }
}
