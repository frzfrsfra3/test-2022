<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_unit', function (Blueprint $table) {
            // dd(Blueprint);
            $table->id();
            $table->unsignedBiginteger('product_id')->unsigned()->nullable();
            $table->unsignedBiginteger('unit_id')->unsigned()->nullable();
            // $table->decimal('amount',13,4)->nullable();
            $table->integer('amount')->nullable();
            $table->foreign('product_id')->references('id')
                 ->on('products')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')
                ->on('units')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_unit');
    }
};
