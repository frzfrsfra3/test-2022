<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Image as Image2;
// use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function hl_uploadFileTo($file,$path)
    {

        $name= $file->getClientOriginalName();

        $file->move($path,$path.'/'. $name);

        return [
            'media_path' => $file,
            'media_url' => $this->getMediaUrl($file)
        ];


    }
public    function getMediaUrl($path)
    {
        return url('/public/'.$path);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   /* public function store(Request $request)
    {
        $image       = $request->file('image');
        $filename    = $image->getClientOriginalName();

        //Fullsize
        $image->move(public_path().'/full/',$filename);

        $image_resize = Image::make(public_path().'/full/'.$filename);
        $image_resize->fit(300, 300);
        $image_resize->save(public_path('thumbnail/' .$filename));

        $product= new Product();
        $product->name = $request->name;
        $product->image = $filename;
        $product->save();
        if($request->o_type == 'user')
            $user = User::find($request->o_id);
        if($request->o_type == 'product')
            $product = Product::find($request->o_id);
            $image = $manager->make('public/foo.jpg')->resize(300, 200);
            // dd($request);
    }
    */
    public function store(Request $request)
    {

        if($request->o_type == 'user')
        {
            $user = User::find($request->o_id);
        //     if ($request->hasFile('path'))
        //    {
            //    hl_deleteFile($partner->avatar_path);
                // $image_data = $this->hl_uploadFileTo($request->path, 'user/');
                Image::create([
                    // 'path'=>  $request->path->getClientOriginalName() ,
                    'path'=>  $request->path,
                    'description'=> $request->description,
                    'created_at'=>  \Carbon\Carbon::now(),
                    'updated_at'=>  \Carbon\Carbon::now(),
                    'o_id' => $user->id,
                    'o_type'=> 'user',

                ]);
            // }

            // dd($request->path->getClientOriginalName());

        }
            if($request->o_type == 'product')
         {
            $product = Product::find($request->o_id);
            // dd($product->id);
            // if ($request->hasFile('path'))
            // {
             //    hl_deleteFile($partner->avatar_path);
                //  $image_data = $this->hl_uploadFileTo($request->path, 'product/');
                 Image::create([
                     'path'=>  $request->path ,

                     'description'=> $request->description,
                     'o_id' => $product->id,
                     'o_type'=> 'product',


                     'created_at'=>  \Carbon\Carbon::now(),
                     'updated_at'=>  \Carbon\Carbon::now(),


                 ]);
            //  }

         }
         return "success";
            // $image = $manager->make('public/foo.jpg')->resize(300, 200);
            // dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
