<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductUnit;
use Illuminate\Support\Facades\Storage;
use App\Models\Image;
// use App\Models\Favourite;
// use Intervention\Image\ImageManagerStatic as Image;
use URL;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        //
        // return $this->hasMany('App\Models\Unit','unit_id','id');
        return $this->belongsToMany('App\Models\Unit', 'product_unit', 'product_id', 'unit_id')->withTimestamps();

    }
    public function sum($productId)
    {
       $pr= Product::with('units')->find($productId);
       $sum= ProductUnit::get()->where('product_id',$productId)->toArray();
    //    dd($sum[2]);
    // dd($pr['units'][0]['modifier']);
    // dd(count($pr['units'][0]['modifier']));
    // dd(count($pr['units']));
    // dd(count($sum));
    $total=0;
    if(count($sum)>0)
        {
    $sum=array_combine(range(0,count($sum)-1),$sum);
    // dd($sum);

    for ($i=0;$i<count($sum);$i++)
    {
        $total+=$sum[$i]['amount']*$pr['units'][$i]['modifier'];
    }

    }
        // return count($sum);
        return $total;
    }
    public function getTotalQuantityAttribute()
    {
        // return count($this->units());
        // return $this->units();
        return $this->sum($this->id);
    }

    public function getImagePathAttribute()
    {
        // $url = $image->store('hall_album/images');
        // $insert = [
        //     'hall_id' => $this->id,
        //     'media_path' => $url,
        //     'media_url' => Storage::url($url),
        //     'media_type' => 'image',
        // ];
        // return null;
        // return Storage::url($this->image_path);
        $imagepath=Image::get()->where('o_type','product')->where('o_id',$this->id)->first();
        // dd($imagepath->path);

        // dd($imagepath);
        return $imagepath==null ? 'null' : $imagepath->path;
        // return $imagepath==null ? 'null' : public_path('product\\'.$imagepath->path);
        // return null;
    }

    public function photo() {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function Total($unitId)
    {
        $pr= Unit::find($unitId);
        $sum= ProductUnit::get()->where('product_id',$this->id)->where('unit_id',$unitId)->toArray();
        // dd($sum);
        // dd($pr->modifier);
        // dd(count($pr));
        // dd($this->getTotalQuantityAttribute());
        // I added this condition to trace when the user adds quantity to products maybe he could add many quantity to the product ,thus he will see the dates in the future
        // if (count($sum)>0)
//    {    $sum=array_combine(range(0,count($sum)-1),$sum);
//         // dd($test);
//         // dd($this->id);
//         //    dd($sum[2]['amount']);

//         $total=0;
//         for ($i=0;$i<count($sum);$i++)
//         {
//             $total+=$sum[$i]['amount']*$pr->modifier;
//             // $total+=$sum[$i]['amount'];
//         }
//             // return count($sum);
//             return (double) $this->getTotalQuantityAttribute()/$total;

//     }
//     else {
        return $this->getTotalQuantityAttribute()/$pr->modifier;
    // }
    }
}
