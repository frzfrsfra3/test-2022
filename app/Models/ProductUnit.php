<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
    use HasFactory;
    protected $table="product_unit";
    protected $fillable = [
        'amount',
        'product_id',
        'unit_id'

    ];
    public function units()
    {
        $this->hasMany('App\Models\Unit','unit_id');
    }

}
