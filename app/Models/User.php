<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use App\Models\Image;
// use App\Models\Favourite;
// use Intervention\Image\ImageManagerStatic as Image;
use URL;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'image_path'
    ];

    public function getImagePathAttribute()
    {
        $imagepath=Image::get()->where('o_type','user')->where('o_id',$this->id)->first();
        // dd($imagepath->path);

        // dd($imagepath);
        return $imagepath==null ? 'null' : $imagepath->path;
    }

    public function photo() {
        return $this->morphOne('App\Models\Image', 'imageable');
    }
}
